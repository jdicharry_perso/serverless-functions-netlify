const res = document.querySelector(".result");

const fetchProducts = async () => {
  try {
    // const { data } = await axios.get("/api/3-airtable");
    const { data } = await axios.get("/api/3-airtable-complete");
    const products = data
      .map((product) => {
        const { id, image, name, url, price } = product;
        return `
        <a href="product.html?id=${id}" class="product">
        <img
          src="${url}"
          alt="${name}"
        />
        <div class="info">
          <h5>${name}</h5>
          <h5 class="price">€${price}</h5>
        </div>
      </a>`;
      })
      .join("");
    res.innerHTML = products;
  } catch (error) {
    res.innerHTML = `<h4>ERREUR DE CHARGEMENT DES DONNEES AIRTABLE</h4>`;
  }
};

fetchProducts();
