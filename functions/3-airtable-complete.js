require("dotenv").config();
const Airtable = require("airtable-node");

const airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY })
  .base(process.env.AIRTABLE_BASE_ID)
  .table("products");

exports.handler = async (event, context) => {
  const { id } = event.queryStringParameters;

  if (id) {
    try {
      const product = await airtable.retrieve(id);

      if (product.error) {
        return {
          statusCode: 404,
          body: `Pas de produit avec l'id ${id}`,
        };
      }

      return {
        statusCode: 200,
        body: JSON.stringify(product, null, 2),
      };
    } catch (error) {
      return {
        statusCode: 500,
        body: "SERVER ERROR",
      };
    }
  }
  try {
    const { records } = await airtable.list();
    const products = records.map((product) => {
      const { id } = product;
      const { image, name, price } = product.fields;
      const url = image[0].url;
      return { id, image, name, url, price };
    });

    return {
      statusCode: 200,
      body: JSON.stringify(products),
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: "SERVER ERROR",
    };
  }
};
